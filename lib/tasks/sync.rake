task :reload_sources do
  BW::Sync.privious_process_check
  BW::Utils::FTPAccess.remote_sources do |access, entity|
    next if BW::VersionControl.new(entity).actual_source_version?
    BW::DBLogger.new(bw_type: 'SOURCE DOWNLOAD', bw_name: entity.file_name).follow_to do
      entity.reload_source!(access)
    end
  end
end

namespace :db do

  task :upload_sources => :reload_sources do
    BW::DBLogger.new(bw_type: 'ITERATION').follow_to{ BW::EntityCollection.reload_sources! }
    United::Models::YCustomer.sync_with_bw_table
    BW::DBLogger.new(bw_type: 'REFRESH MATERIALIZED VEIWS').follow_to do
      ::ActiveRecord::Base.connection.execute("REFRESH MATERIALIZED VIEW turnover_statistics;
                                               REFRESH MATERIALIZED VIEW turnover_statistics_by_brand;
                                               REFRESH MATERIALIZED VIEW turnover_statistics_by_month;
                                               REFRESH MATERIALIZED VIEW turnover_statistics_by_month_by_salon;
                                               REFRESH MATERIALIZED VIEW evolution_by_salon;")
    end
  end

  task :sync do
    United::Models::YCustomer.sync_with_bw_table
  end

  task :clean_logs do
    BW::Models::Log.delete_all
  end

  task :clean_digests do
    BW::Models::Digest.delete_all
  end

  task :find_fuckup do
    BW::Models::YTurnover.find_each do |item|
      begin
          query = <<-SQL
            SELECT ROUND(t.value_loreal::numeric, 2) FROM (VALUES ('#{item.value_loreal}')) AS t (value_loreal);
          SQL
          ActiveRecord::Base.connection.execute(query)
          puts "SUCCESS WITH #{item.value_loreal}"
      rescue Exception => e
        puts "ITEM #{item.inspect}"
        raise e
      end
    end
  end

end


