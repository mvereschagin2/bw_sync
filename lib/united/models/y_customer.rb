class United::Models::YCustomer < ActiveRecord::Base
  self.table_name = 'y_customers'

  validates_presence_of :code_bw

  #по идее Y может быть только один, но возможны дубликаты которые будут отбрасываться
  has_one :bw_y_customer, class_name: '::BW::Models::YCustomer', foreign_key: 'code_bw', primary_key: 'code_bw'

  #после обновления записей в bw-таблице отот метод синхронизирует наши Yки с ней
  def self.sync_with_bw_table
    BW::DBLogger.new(bw_type: 'SYNC DATA', bw_name: 'y_customers').follow_to do
      includes(:bw_y_customer).find_each do |record|
        BW::SyncRecord.new(record).start!
      end
    end
  end

end

