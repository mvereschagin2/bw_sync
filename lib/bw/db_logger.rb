class BW::DBLogger

  def self.notify!(msg)
    puts("[#{DateTime.current}]".colorize(:light_green) + " #{msg}".colorize(:light_yellow))
  end

  def initialize(options)
    @bw_type = options[:bw_type] || ''
    @bw_name = options[:bw_name] || ''
    @process_pid = Process.pid
  end

  def follow_to
    begin
      start!
      yield
      finish!
    rescue Exception => e
      error!(e)
    end
  end

  def start!
    lets_log!(status: 'START', process_pid: @process_pid)
  end

  def finish!
    lets_log!(status: 'FINISH')
  end

  def error!(e)
    lets_log!(status: 'ERROR', error: error_message(e))
    self.class.notify! error_message(e)
    email_notify!      error_message(e)
  end

  def lets_log!(params)
    params = params.merge(log_params)
    BW::Models::Log.create(params)
    self.class.notify! "#{params[:status]} --- #{params[:bw_type]} --- #{params[:bw_name]}, pid: #{@process_pid}"
  end

  def log_params
    {bw_type: @bw_type, bw_name: @bw_name, memory_usage: memory_measure}
  end

  private

  def email_notify!(msg)
    return if ENV["NOTIFICATION_MAIL_FROM"].blank? || ENV["NOTIFICATION_MAIL_TO"].blank?
    Mail.deliver do
      from    ENV["NOTIFICATION_MAIL_FROM"]
      to      ENV["NOTIFICATION_MAIL_TO"]
      subject "bw_sync error!"
      body    msg
    end
  end

  def error_message(e)
    e.message + "\n" + e.backtrace.join("\n")
  end

  def memory_measure
    `ps -o rss= -p #{$$}`
  end

end