class BW::SyncRecord

  attr_reader :record, :bw_record

  def initialize(record)
    @record = record
    @bw_record = record.bw_y_customer
  end

  def start!
    return unless check_bw_record
    if record_attrs != bw_record_attrs
      if record.update(bw_record_attrs)
        BW::DBLogger.notify! "Update y_customer #{record.code_bw} with attributes #{bw_record_attrs}"
      else
        BW::DBLogger.notify! "Error update y_customer #{record.code_bw} with attributes #{bw_record_attrs}"
      end
    end
  end

  private

  def record_attrs
    common_attributes(record)
  end

  def bw_record_attrs
    common_attributes(bw_record)
  end

  def check_bw_record
    if bw_record
      true
    else
      BW::DBLogger.notify! "Record #{record.code_bw} not found for bw table!"
      false
    end
  end

  def common_attributes(item)
    item.slice( :code_bw,
                :name,
                :partner_id ,
                :partner_name,
                :hierarchy2_code,
                :hierarchy2_name,
                :type_code,
                :type_name,
                :address_city,
                :address_street,
                :code_partner_erp
    )
  end

end