class BW::EntityCollection

  extend BW::Utils::Mapper

  def self.all
    entities_arguments.map do |args|
      Entity.new(*args)
    end
  end

  def self.reload_sources!
    all.each do |entity|
      next if BW::VersionControl.new(entity).actual_db_version?
      BW::DBLogger.new(bw_type: 'REFRESH TABLE RECORDS', bw_name: entity.table_name).follow_to do
        entity.upload_source!
      end
    end
  end

  #объект, хранящий имя таблицы, модели, ее полей и имя файла источника
  class Entity

    include BW::Utils::CSVOptions

    attr_reader :file_name, :model, :table_name, :columns, :month_check, :skip, :overwrite_table, :date_columns

    def initialize(file_name, model, options={})
      @file_name = file_name
      @model = model
      @table_name = model.table_name
      @columns = options[:csv_columns]
      @skip = options[:skip_csv_values]
      @month_check = options[:month_check]
      @overwrite_table = options[:overwrite_table]
      @date_columns = options[:date_columns] || {}
    end

    def reload_source!(access)
      system("curl -u #{access.credentials} '#{access.full_path}#{file_name}' -o #{file_path}")
      BW::Source.prepare(file_name)
    end

    def upload_source!
      ActiveRecord::Base.transaction do
        BW::DBCleaner.new(self).start!
        with_silient_log{ update_table! }
        BW::SHAMark.new(self).set!
      end
    end

    def update_table!
      BW::DBLogger.new(bw_type: 'INSERT LINES', bw_name: @table_name).follow_to do
        CSV.foreach(file_path, csv_options) do |line|
          BW::Item.new(self, line).insert!
        end
        print "\n"
      end
    end

    def file_path
      BW::Utils::Mapper::SOURCES_PATH + @file_name
    end

    def hexdigest
      Digest::SHA256.file(file_path).hexdigest
    end

    def month_check?
      @month_check.present?
    end

    def day_check?
      !month_check?
    end

    private

    def with_silient_log
      old_logger = ActiveRecord::Base.logger
      ActiveRecord::Base.logger = nil
      yield
      ActiveRecord::Base.logger = old_logger
    end

  end

end