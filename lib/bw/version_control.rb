class BW::VersionControl

  def initialize(entity)
    @entity = entity
  end

  #Смотрим, не пора ли обновить файлы (возвращаем false если пора)
  def actual_source_version?
    #загружаем любой файл если его нет
    return false if !Pathname.new(@entity.file_path).exist?

    if @entity.day_check?
      actual_day_source?
    else
      actual_month_source?
    end
  end

  #Смотрим, не пора ли обновить данные в базе
  def actual_db_version?
    @table_digests = BW::Models::Digest.where(table_name:  @entity.table_name, source: @entity.file_path)
    return true if upload_was_in_range?
    @last_digest = @table_digests.order(:created_at).last
    return false if !@last_digest
    source_was_not_changed?
  end

  private

  #дневные перезагружаем при каждом запросе
  def actual_day_source?
    false if @entity.day_check?
  end

  def actual_month_source?
    BW::Models::Log.where(bw_type: 'Source', bw_name: @entity.file_name, created_at: this_month).present? if @entity.month_check?
  end

  def select_range
    @entity.month_check? ? this_month : today
  end

  def today
    (Date.current.beginning_of_day..Date.current.end_of_day)
  end

  def this_month
    (Date.current.beginning_of_month..Date.current.end_of_month)
  end

  def source_was_not_changed?
    if @last_digest.code == @entity.hexdigest
      message!(:source_not_changed)
      return true
    end
  end

  def upload_was_in_range?
    if @table_digests.where(created_at: select_range).present?
      message!(:upload_was_in_range)
      return true
    end
  end

  def message!(type)
    return (puts upload_was_in_range_message) if type == :upload_was_in_range
    return (puts source_was_changed_message) if type == :source_not_changed
  end

  def upload_was_in_range_message
    BW::DBLogger.notify! "The last upload #{@entity.file_path} to #{@entity.table_name} table was #{@entity.month_check? ? 'this month' : 'today'}"
  end

  def source_was_changed_message
    BW::DBLogger.notify! "The source #{@entity.file_path} of #{@entity.table_name} table was not changed"
  end

end