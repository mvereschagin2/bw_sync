class BW::Models::YCustomer < ActiveRecord::Base
  self.table_name = 'bw_y_customers'
  
  before_save :partner_handling
  
  private
  
  def partner_handling
    unless self.code_bw =~ /^y.*/i
      self.partner_id = "P001"
      self.partner_name = "P001"
    end
  end
  
end