class BW::Models::YTurnover < ActiveRecord::Base
  self.table_name = 'bw_y_turnovers'

  before_save :filter_negative_values

  private

  #обрабатываем кейс с минусом в конце для отрицательных значений (переставляем минус в начало)
  def filter_negative_values
    %w(value_loreal units value_partner).each do |field|
      if /(?<double_precision>\d*\.\d*)-/ =~ self.send(field)
        self.send(field + '=', "-#{double_precision}")
      end
    end
  end

end