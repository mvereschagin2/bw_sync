module BW::Sync

  class SyncError < ::RuntimeError; end

  def self.privious_process_check
    raise SyncError, sync_error_msg if iteration_already_running?
  end

  def self.iteration_already_running?
    !!previous_process_pid && `ps -a | grep '#{previous_process_pid}.*ruby'`.present?
  end

  def self.previous_process_pid
    BW::Models::Log.where("bw_logs.process_pid IS NOT NULL AND bw_logs.process_pid <> '#{Process.pid}'")
        .order(:created_at)
        .last.try(:process_pid)
  end

  def self.sync_error_msg
    "The process with pid #{previous_process_pid} already running. Please try again later or kill this process"
  end

end