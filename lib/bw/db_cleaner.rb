class BW::DBCleaner

  include BW::Utils::CSVOptions

  class DBCleanerError < ::RuntimeError; end
  class DateColumnsError < DBCleanerError; end

  def initialize(entity)
    @entity = entity
    @year_col_num = entity.date_columns[:year]
    @month_col_num = entity.date_columns[:month]
  end

  def start!
    return full_clean! if @entity.overwrite_table == :full
    filtered_clean!
  end

  def full_clean!
    @entity.model.delete_all
    BW::DBLogger.notify! "FULL CLEAN #{@entity.table_name}"
  end

  def filtered_clean!
    raise DateColumnsError, date_column_error_message unless date_columns_present?
    parse_uniq_dates
    db_filtered_clean!
  end

  private

  def date_columns_present?
    (@year_col_num.present? && @month_col_num.present?)
  end

  def parse_uniq_dates
    @date_values = []
    CSV.foreach(@entity.file_path, csv_options) do |line|
      @date_values = add_uniq_data(line)
    end
  end

  def add_uniq_data(line)
    (@date_values << {year: line[@year_col_num], month: line[@month_col_num]}).uniq
  end

  def db_filtered_clean!
    @date_values.each do |date|
      if date[:year].present? && date[:month].present?
        @entity.model.where(date_year: date[:year], date_month: date[:month]).delete_all
        BW::DBLogger.notify! "FILTERED CLEAN #{@entity.table_name} FOR #{date[:year]} YEAR AND #{date[:month]} MONTH"
      end
    end
  end

  def date_column_error_message
    "Year and/or month column numbers are not specified for #{@entity.model.name}"
  end

end