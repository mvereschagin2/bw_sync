class BW::Item

  attr_reader :table_name
  attr_accessor :values

  def initialize(entity, values)
    @entity = entity
    @columns = @entity.columns
    @values = filter_values(values)
    @attributes = construct_attributes.with_indifferent_access
  end

  def insert!
    return if empty_values? || failure_record?
    record = @entity.model.create(@attributes)
    dot_print(record)
  end

  private

  #используется когда в csv строке надо пропустить несколько значений подряд
  def filter_values(values)
    return values unless @entity.skip.is_a?(Range)
    tmp = []
    values.each_index{|i| tmp << values[i] unless @entity.skip.include?(i)}
    tmp.map{ |v| v.nil? ? '' : v }
  end

  def filter_missing_data_string(values)
    values.map{ |v| v.nil?  ? '' : v }
  end

  def failure_record?
    @attributes[:code_bw] == 'Y00000_000' || @attributes[:name] == "Missing Master Data TPS"
  end

  def empty_values?
    @values.compact.empty?
  end

  def construct_attributes
    tmp = {}
    @columns.each_with_index do |c,index|
      tmp.merge!(c => values[index])
    end
    tmp
  end

  def dot_print(record)
    print '.' if record.id%1000 == 0
  end

end