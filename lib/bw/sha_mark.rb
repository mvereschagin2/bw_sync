class BW::SHAMark

  def initialize(entity)
    @entity = entity
  end

  def set!
    BW::Models::Digest.create(table_name: @entity.table_name, source: @entity.file_path, code: @entity.hexdigest)
  end

end