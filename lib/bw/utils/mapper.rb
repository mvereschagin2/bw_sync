module BW::Utils::Mapper

  SOURCES_PATH = 'store/'

  MATERIALS_FILE      = 'Z_DPP_MATERIALS.csv'.freeze
  CUSTOMERS_FILE      = 'Z_DPP_Y_CUSTOMERS.csv'.freeze
  TURNOVER_MONTH_FILE = 'Z_DPP_Y_TURNOVER_MONTH.csv'.freeze
  TURNOVER_YEAR_FILE  = 'Z_DPP_Y_TURNOVER_YEAR.csv'.freeze

  def entities_arguments
    [[MATERIALS_FILE, BW::Models::YMaterial, csv_columns: [
                        'signature_id',
                        'signature_name',
                        'brand_id',
                        'brand_name',
                        'sub_brand_id',
                        'sub_brand_name',
                        'reference_id',
                        'reference_name',
                        'nuance_id',
                        'nuance_name',
                        'axe_id',
                        'axe_name',
                        'sub_axe_id',
                        'sub_axe_name',
                        'class_id',
                        'class_name',
                        'function_id',
                        'function_name',
                        'material_id',
                        'material_name'
                    ], overwrite_table: :full],
     [CUSTOMERS_FILE, BW::Models::YCustomer, csv_columns: [
                        'partner_id',
                        'partner_name',
                        'hierarchy2_code',
                        'hierarchy2_name',
                        'code_loreal',
                        'code_bw',
                        'name',
                        'type_code',
                        'type_name',
                        'address_city',
                        'address_street',
                        'code_partner_erp'
                    ], overwrite_table: :full],
     [TURNOVER_MONTH_FILE, BW::Models::YTurnover, csv_columns: [
                        'code_bw',
                        'date_year',
                        'date_month',
                        'signature_id',
                        'material_id',
                        'value_loreal',
                        'units',
                        'value_partner'
                    ], skip_csv_values: (4..11),
                       overwrite_table: :by_month,
                       date_columns: {year: 1, month: 2}
     ],
     [TURNOVER_YEAR_FILE, BW::Models::YTurnover, csv_columns: [
                        'code_bw',
                        'date_year',
                        'date_month',
                        'signature_id',
                        'material_id',
                        'value_loreal',
                        'units',
                        'value_partner'
                    ], skip_csv_values: (4..11),
                       month_check: true,
                       overwrite_table: :by_month,
                       date_columns: {year: 1, month: 2}]
    ]
  end


end