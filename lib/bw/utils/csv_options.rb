module BW::Utils::CSVOptions
  def csv_options
    {:col_sep => ';', :encoding => 'ISO-8859-5', :quote_char => "|"}
  end
end