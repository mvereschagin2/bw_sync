class BW::Utils::FTPAccess

  class << self

    def credentials
      "#{ENV['FTP_LOGIN']}:#{ENV['FTP_PASSWORD']}"
    end

    def full_path
      ENV['FTP_FULL_PATH']
    end

    def remote_sources
      BW::EntityCollection.all.each do |entity|
        yield self, entity
      end
    end

  end

end