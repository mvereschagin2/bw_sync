class BW::Utils::Connection

  def self.establish!
    ::ActiveRecord::Base.establish_connection load_config_data
  end

  def self.load_config_data
    YAML.load(ERB.new(File.read 'db/config.yml').result)[ENV['ENVIRONMENT']]
  end

end