class BW::Source
  def self.prepare(file)
    #убираем нечитабельный заголовок из csv
    lines = File.readlines("store/#{file}")
    lines[0] = ''
    File.open("store/#{file}", 'w'){ |f| f.write(lines.join) }
  end
end