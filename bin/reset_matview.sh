#!/usr/bin/env bash

key="$1"
VERSION="$3"

case $key in
    -e|--environment)
    ENVIRONMENT="$2"
    ;;
    *)
    ENVIRONMENT="development"
    ;;
esac

bundle exec rake db:migrate:down VERSION=20170323125754 db="${ENVIRONMENT}"
bundle exec rake db:migrate:up   VERSION=20170323125754 db="${ENVIRONMENT}"
