require 'active_support/all'
require 'active_record_migrations'
require 'dotenv/load'
require 'csv'
require 'yaml'
require 'digest'
require 'colorize'
require 'mail'

module BW
  module Utils; end
  module Models; end

  def self.required_files
    %w(config/active_record.rb lib/united/models/*.rb lib/bw/models/*.rb lib/bw/utils/*.rb lib/bw/utils/*.rb lib/bw/*.rb)
  end
end

module United
  module Models; end
end

BW.required_files.each do |path|
  Dir.glob(path).each{|file| require_relative file}
end

Mail.defaults do
  delivery_method :smtp,
                  address:         'smtp.mandrillapp.com',
                  port:            587,
                  domain:          'e-academie.ru',
                  user_name:       "PPD L'Oréal Russia",
                  password:        ENV["SMTP_DOMAIN_PASSWORD"],
                  authentication:  'plain'
end

BW::Utils::Connection.establish!






