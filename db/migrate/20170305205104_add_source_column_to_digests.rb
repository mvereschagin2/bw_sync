class AddSourceColumnToDigests < ActiveRecord::Migration
  def change
    add_column :bw_hexdigests, :source, :string
  end
end
