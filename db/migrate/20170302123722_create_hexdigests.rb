class CreateHexdigests < ActiveRecord::Migration
  def change
    create_table :bw_hexdigests do |t|
      t.column :table_name, :string
      t.column :code, :string
      t.timestamps null: false
    end
  end
end
