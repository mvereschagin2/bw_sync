class CreateStatusColumnToLog < ActiveRecord::Migration
  def change
    add_column :bw_logs, :status, :string
    add_column :bw_logs, :error,  :string
    add_column :bw_logs, :memory_usage, :string
  end
end
