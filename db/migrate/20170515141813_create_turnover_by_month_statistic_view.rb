class CreateTurnoverByMonthStatisticView < ActiveRecord::Migration
  def up
    #удаляем старую реализацию (теперь группируем по месяцам), добавляем еще одну вьюху и все вьюхи делаем материальными
    execute <<-SQL

      DROP MATERIALIZED VIEW IF EXISTS turnover_statistics CASCADE;

      CREATE MATERIALIZED VIEW turnover_statistics AS
        SELECT t.code_bw, m.signature_id, m.signature_name, t.date_month, t.date_year, SUM(ROUND(t.value_loreal::numeric, 2)) as value_loreal_sum
        FROM bw_y_turnovers t inner join bw_y_materials m
        USING(material_id)
        GROUP BY t.code_bw, t.date_month, t.date_year, m.signature_id, m.signature_name;

      CREATE MATERIALIZED VIEW turnover_statistics_by_brand AS
        SELECT ts.code_bw,  SUM(case when  ts.signature_id = 'C2' then ts.value_loreal_sum else 0.0 end) as lp,
                             SUM(case when  ts.signature_id = 'C4' then ts.value_loreal_sum else 0.0 end) as kr,
                             SUM(case when  ts.signature_id = 'C6' then ts.value_loreal_sum else 0.0 end) as mx,
                             SUM(case when  ts.signature_id = 'C8' then ts.value_loreal_sum else 0.0 end) as rd,
                             SUM(case when  ts.signature_id = 'CC' then ts.value_loreal_sum else 0.0 end) as cr,
                             SUM(case when  ts.signature_id = 'CD' then ts.value_loreal_sum else 0.0 end) as dc
        FROM turnover_statistics ts GROUP BY ts.code_bw;

      CREATE MATERIALIZED VIEW turnover_statistics_by_month AS
        SELECT ts.code_bw,  ts.date_year, ts.signature_id,
                             SUM(case when  ts.date_month = '01' then ts.value_loreal_sum else 0.0 end) as Jan,
                             SUM(case when  ts.date_month = '02' then ts.value_loreal_sum else 0.0 end) as Feb,
                             SUM(case when  ts.date_month = '03' then ts.value_loreal_sum else 0.0 end) as Mar,
                             SUM(case when  ts.date_month = '04' then ts.value_loreal_sum else 0.0 end) as Apr,
                             SUM(case when  ts.date_month = '05' then ts.value_loreal_sum else 0.0 end) as May,
                             SUM(case when  ts.date_month = '06' then ts.value_loreal_sum else 0.0 end) as Jun,
                             SUM(case when  ts.date_month = '07' then ts.value_loreal_sum else 0.0 end) as Jul,
                             SUM(case when  ts.date_month = '08' then ts.value_loreal_sum else 0.0 end) as Aug,
                             SUM(case when  ts.date_month = '09' then ts.value_loreal_sum else 0.0 end) as Sep,
                             SUM(case when  ts.date_month = '10' then ts.value_loreal_sum else 0.0 end) as Oct,
                             SUM(case when  ts.date_month = '11' then ts.value_loreal_sum else 0.0 end) as Nov,
                             SUM(case when  ts.date_month = '12' then ts.value_loreal_sum else 0.0 end) as Dec,
                             SUM(ts.value_loreal_sum) as Total

        FROM turnover_statistics ts GROUP BY ts.code_bw, ts.date_year, ts.signature_id;
    SQL
  end

  def down
    execute <<-SQL
      DROP MATERIALIZED VIEW IF EXISTS turnover_statistics  CASCADE;
    SQL
  end
end
