class AddProcessPidToLogger < ActiveRecord::Migration
  def change
    add_column :bw_logs, :process_pid, :string
  end
end
