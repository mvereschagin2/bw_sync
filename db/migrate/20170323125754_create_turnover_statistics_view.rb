class CreateTurnoverStatisticsView < ActiveRecord::Migration
  def up
    #turnover_statistics Формирует статистику общей стоимости отгрузок (value_loreal) по одному бренду (signature_id) Y-ка (code_bw)
    #turnover_statistics_by_code_bw Пересобирает предыдущую таблицу для просмотра стоимости по каждому бренду для одного Y-ка
    # В случае необходимости отредактировать  миграцию, вместо написания новой (matview генерится несколько минут при большом объеме данных) можно воспользоваться командой
    # sh bin/reset_matview.sh -e my_current_environment (грязный хак, придется запустить на каждой машине)
    execute <<-SQL

      CREATE MATERIALIZED VIEW turnover_statistics AS
      SELECT t.code_bw, m.signature_id, m.signature_name, SUM(ROUND(t.value_loreal::numeric, 2)) as value_loreal_sum
      FROM bw_y_turnovers t inner join bw_y_materials m
      USING(material_id)
      GROUP BY t.code_bw, m.signature_id, m.signature_name;


      CREATE OR REPLACE VIEW turnover_statistics_by_code_bw AS
      SELECT ts.code_bw,  SUM(case when  ts.signature_id = 'C2' then ts.value_loreal_sum else 0.0 end) as lp,
                           SUM(case when  ts.signature_id = 'C4' then ts.value_loreal_sum else 0.0 end) as kr,
                           SUM(case when  ts.signature_id = 'C6' then ts.value_loreal_sum else 0.0 end) as mx,
                           SUM(case when  ts.signature_id = 'C8' then ts.value_loreal_sum else 0.0 end) as rd,
                           SUM(case when  ts.signature_id = 'CC' then ts.value_loreal_sum else 0.0 end) as cr,
                           SUM(case when  ts.signature_id = 'CD' then ts.value_loreal_sum else 0.0 end) as dc
      FROM turnover_statistics ts GROUP BY ts.code_bw;
    SQL
  end

  def down
    execute <<-SQL
      DROP VIEW IF EXISTS turnover_statistics_by_code_bw;
      DROP MATERIALIZED VIEW  IF EXISTS turnover_statistics;
    SQL
  end
end
