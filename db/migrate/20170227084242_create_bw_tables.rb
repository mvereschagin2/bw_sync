class CreateBwTables < ActiveRecord::Migration
  def change
    create_table :bw_y_materials do |t|
      t.column :material_id  , :string
      t.column :material_name, :string
      t.column :function_id  , :string
      t.column :class_id     , :string
      t.column :class_name   , :string
      t.column :sub_axe_id   , :string
      t.column :sub_axe_name , :string
      t.column :nuance_id    , :string
      t.column :nuance_name  , :string
      t.column :reference_id , :string
      t.column :brand_id     , :string
      t.column :brand_name   , :string
      t.column :sub_brand_id  , :string
      t.column :sub_brand_name, :string
      t.column :signature_id  , :string
      t.column :signature_name, :string
      t.timestamps null: false
    end

    create_table :bw_y_customers do |t|
      t.column :code_bw      , :string
      t.column :name         , :string
      t.column :code_loreal  , :string
      t.column :partner_id   , :string
      t.column :partner_name , :string
      t.column :hierarchy2_code, :string
      t.column :hierarchy2_name, :string
      t.column :type_code    , :string
      t.column :type_name    , :string
      t.column :address_city , :string
      t.column :address_street  , :string
      t.column :code_partner_erp, :string
      t.timestamps null: false
    end

    create_table :bw_y_turnovers do |t|
      t.column :code_bw      , :string
      t.column :material_id  , :string
      t.column :value_loreal , :string
      t.column :partner_name , :string
      t.column :units        , :string
      t.column :value_partner , :string
      t.timestamps null: false
    end

    add_column :bw_y_turnovers, :date_month, :string
    add_column :bw_y_turnovers, :date_year, :string
    add_column :bw_y_turnovers, :signature_id, :string
    add_column :bw_y_materials, :reference_name, :string
    add_column :bw_y_materials, :axe_id, :string
    add_column :bw_y_materials, :axe_name, :string
    add_column :bw_y_materials, :function_name, :string

  end
end
