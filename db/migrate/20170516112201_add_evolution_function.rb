class AddEvolutionFunction < ActiveRecord::Migration

  def up
    execute <<-SQL

      CREATE OR REPLACE function evolution(diff numeric[]) returns numeric AS $$

        SELECT CASE
           WHEN diff[1] IS NOT NULL AND diff[2] IS NOT NULL AND diff[2] != 0.0
             then round(diff[1]/(diff[2]/100)) - 100
           ELSE
             NULL
           END;

      $$ LANGUAGE SQL;

      CREATE MATERIALIZED VIEW evolution_by_salon AS
        SELECT
          salon_id,
          signature_id,
          evolution(array_agg(jan ORDER BY date_year DESC)) AS jan,
          evolution(array_agg(feb ORDER BY date_year DESC)) AS feb,
          evolution(array_agg(mar ORDER BY date_year DESC)) AS mar,
          evolution(array_agg(apr ORDER BY date_year DESC)) AS apr,
          evolution(array_agg(may ORDER BY date_year DESC)) AS may,
          evolution(array_agg(jun ORDER BY date_year DESC)) AS jun,
          evolution(array_agg(jul ORDER BY date_year DESC)) AS jul,
          evolution(array_agg(aug ORDER BY date_year DESC)) AS aug,
          evolution(array_agg(sep ORDER BY date_year DESC)) AS sep,
          evolution(array_agg(oct ORDER BY date_year DESC)) AS oct,
          evolution(array_agg(nov ORDER BY date_year DESC)) AS nov,
          evolution(array_agg(dec ORDER BY date_year DESC)) AS dec,
          evolution(array_agg(total ORDER BY date_year DESC)) AS total
        FROM turnover_statistics_by_month_by_salon
        WHERE date_year IN (date_part('year', CURRENT_DATE)::varchar, (date_part('year', CURRENT_DATE)-1)::varchar)
        GROUP BY salon_id, signature_id;

    SQL
  end

  def down
    execute <<-SQL
       DROP MATERIALIZED VIEW IF EXISTS evolution_by_salon;
       DROP function IF EXISTS evolution(numeric[]);
    SQL
  end

end
