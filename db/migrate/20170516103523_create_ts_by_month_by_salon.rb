class CreateTsByMonthBySalon < ActiveRecord::Migration
  def up
    execute <<-SQL

      CREATE MATERIALIZED VIEW turnover_statistics_by_month_by_salon AS

        SELECT
          salon_id,
          array_agg(code_bw) AS code_bw,
          signature_id,
          date_year,
          sum(jan) AS jan,
          sum(feb) AS feb,
          sum(mar) AS mar,
          sum(apr) AS apr,
          sum(may) AS may,
          sum(jun) AS jun,
          sum(jul) AS jul,
          sum(aug) AS aug,
          sum(sep) AS sep,
          sum(oct) AS oct,
          sum(nov) AS nov,
          sum(dec) AS dec,
          sum(total) as total
        FROM y_customers INNER JOIN turnover_statistics_by_month USING (code_bw)
        GROUP BY salon_id, signature_id, date_year;

    SQL
  end

  def down
    execute <<-SQL
       DROP MATERIALIZED VIEW IF EXISTS turnover_statistics_by_month_by_salon;
    SQL
  end
end
