class CreateBwSourceLog < ActiveRecord::Migration
  def change
    create_table :bw_logs do |t|
      t.column :bw_type,    :string
      t.column :bw_name,    :string
      t.timestamps null: false
    end
  end
end
