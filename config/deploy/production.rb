role :app, %w{user@217.74.39.84}
role :web, %w{user@217.74.39.84}
role :db,  %w{user@217.74.39.84}

server '217.74.39.84', user: 'user', roles: %w{web app db}
set :ssh_options, {
                    verbose: false,
                    port: 2250,
                    forward_agent: true,
                    auth_methods: %w(publickey)
                }

set :deploy_to, '/home/user/applications/bw_sync'
set :branch, ENV['BRANCH'] || 'master'
set :environment, 'production'

set :rbenv_type, :user
set :rbenv_ruby, '2.2.5'
set :rbenv_prefix, "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
set :rbenv_roles, :all


set :keep_releases, 3
