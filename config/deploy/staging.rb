role :app, %w{united_global@5.178.87.109}
role :web, %w{united_global@5.178.87.109}
role :db,  %w{united_global@5.178.87.109}

server '5.178.87.109', user: 'united_global', roles: %w{web app db}

set :ssh_options, {
                    verbose: false,
                    port: 2250,
                    forward_agent: true,
                    auth_methods: %w(publickey)
                }

set :rvm_type, :system
set :rvm_ruby_version, 'ruby-2.2.5@bw_sync'

set :deploy_to, '/home/united_global/www/bw_sync'
set :branch, ENV['BRANCH'] || 'master'
set :rails_env, 'staging'
set :environment, 'staging'

set :keep_releases, 3

set :application, 'bw_sync'
