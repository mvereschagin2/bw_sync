set :log_level, :info
set :application, 'bw_sync'
set :repo_url, 'git@bitbucket.org:mvereschagin2/bw_sync.git'

set :linked_files, %w{db/config.yml log/whenever.log .env}
set :linked_dirs, %w{store}

set :bundle_flags, '--deployment'

set :whenever_environment, proc { 'production' }

namespace :deploy do

  after :finishing, 'deploy:cleanup'

end
