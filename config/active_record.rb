ActiveRecord::Base.tap do |config|
  config.default_timezone = :local
  config.dump_schema_after_migration = false
  #для отображения логирования  ActiveRecord
  #config.logger = Logger.new(STDOUT)
end