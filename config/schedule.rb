env :PATH, ENV['PATH']

set :output, 'log/whenever.log'

=begin
  case ENV['environment']
    when 'production'
      set :output, '/home/user/applications/bw_sync/shared/log/whenever.log'
    when 'staging'
      set :output, '/home/united_global/www/bw_sync/shared/log/whenever.log'
    when 'development'
      set :output, 'log/whenever.log'
  end
=end

every 3.hours do
  rake "db:upload_sources"
end

