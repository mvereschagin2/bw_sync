# README #

### What is this repository for? ###

* Sync bw_sync database with remote csv sources

* Запустить консоль с загруженным модулем BW - bin/console

* Таск для старта bundle exec rake db:upload_sources. При деплое он добавляется в крон и запускается на сервере каждые 3 часа.
    На первом этапе  загружаются все csv файлы кроме месячного, если в этом месяце он уже был загружен  (для этого скрипт смотрит в таблицу bw_logs).
    На втором этапе происходит парсинг файлов и загрузка записей в соответствующую таблицу (каждый файл в свою таблицу или два файла в одну таблицу).
  Таск пропускает загрузку записей из файлов в базу если записи уже были загружены не далее чем за определенный интервал времени (текущий день для дневных и текущий месяц для месячных файлов), о чем свидетельтвует запись в табице bw_digests, при этом
  дополнительным условием является совпадение md5 записи таблицы и md5 файла (который каждый раз загружается заново). При обновлении таблицы в базе
  сначала из таблицы удаляются старые данные (целиком или по определенным месяцам).
    Чтобы форсированно обновить в таблицах данные, необходимо сначала очистить таблицу bw_digests. Для этого можно выполнить скрипт db:clean_digests. Чтобы обновить месячный файл, дополнительно необходимо удалить соответствующую запись в bw_logs (где created_at в текущем месяце).
    Весь процесс, если он стартует по крону, логируется в log/whenever.log (смотреть лучше tail-ом), иначе на стандартный вывод.
    Также скрипт вызовет исключение если в системе уже запущен один процесс.

    После загрузки данных обновляютя данные в таблице y_customers и обновляется materialized view turnover_statistics
    (см. миграцию CreateTurnoverStatisticsView)


* Как найти дубликаты в таблице bw?

  select a.\* from (select bw_y_customers.\*, count(code_bw) over (partition by code_bw) y_count from bw_y_customers) as a where a.y_count > 1;

### How do I get set up? ###

* ensure that curl exists on your system
* bundle install

### Note about migrations ###

* use db/config.yml instead db/database.yml

* The main difference is that instead of rails generate migration (or rails g migration), the generator is implemented as a Rake task. So you should use it like rake "db:new_migration[CreateUser, name birth:date]" (double quotes are required if you use this form). Alternatively you could run it as rake db:new_migration name=CreateUser options="name birth:date".

Just run rake db:new_migration for help on usage.

You can specify the environment by setting the db environment variable:

rake db:migrate db=production